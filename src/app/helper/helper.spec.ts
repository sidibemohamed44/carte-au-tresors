import { getOrientationFromThis, getRatio } from './helper';
import { Orientation, Mouvement } from './mapping';

describe('helper', () => {
    describe('getRatio', () => {
        it('should return ration', () => {
            // Given
            const expected = 10;

            // When
            const result = getRatio(10, 100);
            
            // Then
            expect(result).toEqual(expected);
        });
    });
    describe('getOrientationFromThis', () => {
        describe('initial orientation Est/Ouest', () => {
            it('should return Nord when orientation is Est and move is G', () => {
                // Given
                const orientation = Orientation.EST;
                const mouv = Mouvement.GAUCHE;
                const expected = Orientation.NORD;
    
                // When
                const result = getOrientationFromThis(orientation).forThis(mouv);
    
                // When
                expect(result).toEqual(expected);
            });
            it('should return Sud when orientation is Est and move is D', () => {
                // Given
                const orientation = Orientation.EST;
                const mouv = Mouvement.DROITE;
                const expected = Orientation.SUD;
    
                // When
                const result = getOrientationFromThis(orientation).forThis(mouv);
    
                // When
                expect(result).toEqual(expected);
            });
            it('should return Nord when orientation is Ouest and move is D', () => {
                // Given
                const orientation = Orientation.OUEST;
                const mouv = Mouvement.DROITE;
                const expected = Orientation.NORD;
    
                // When
                const result = getOrientationFromThis(orientation).forThis(mouv);
    
                // When
                expect(result).toEqual(expected);
            });
            it('should return Sud when orientation is Ouest and move is G', () => {
                // Given
                const orientation = Orientation.OUEST;
                const mouv = Mouvement.GAUCHE;
                const expected = Orientation.SUD;
    
                // When
                const result = getOrientationFromThis(orientation).forThis(mouv);
    
                // When
                expect(result).toEqual(expected);
            });
        });

        describe('initial orientation Nord/Sud', () => {
            it('should return Est when orientation is Nord and move is D', () => {
                // Given
                const orientation = Orientation.NORD;
                const mouv = Mouvement.DROITE;
                const expected = Orientation.EST;
    
                // When
                const result = getOrientationFromThis(orientation).forThis(mouv);
    
                // When
                expect(result).toEqual(expected);
            });
            it('should return Ouest when orientation is Nord and move is G', () => {
                // Given
                const orientation = Orientation.NORD;
                const mouv = Mouvement.GAUCHE;
                const expected = Orientation.OUEST;
    
                // When
                const result = getOrientationFromThis(orientation).forThis(mouv);
    
                // When
                expect(result).toEqual(expected);
            });
    
            it('should return Est when orientation is Sud and move is G', () => {
                // Given
                const orientation = Orientation.SUD;
                const mouv = Mouvement.GAUCHE;
                const expected = Orientation.EST;
    
                // When
                const result = getOrientationFromThis(orientation).forThis(mouv);
    
                // When
                expect(result).toEqual(expected);
            });
    
            it('should return Ouest when orientation is Sud and move is D', () => {
                // Given
                const orientation = Orientation.SUD;
                const mouv = Mouvement.DROITE;
                const expected = Orientation.OUEST;
    
                // When
                const result = getOrientationFromThis(orientation).forThis(mouv);
    
                // When
                expect(result).toEqual(expected);
            });
        });
    });
});