import { Orientation, orientationMapping } from "./mapping";

export const getOrientationFromThis = (orientation: Orientation) => ({
    forThis: (move) => orientationMapping[orientation][move]
});

export const getRatio = (value, max) => (value * 100) / max;
