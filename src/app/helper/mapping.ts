export enum Orientation {
    OUEST = 'O',
    EST = 'E',
    SUD = 'S',
    NORD = 'N'
}

export enum Mouvement {
    AVANCER = 'A',
    DROITE = 'D',
    GAUCHE = 'G'
}

export const orientationMapping = {
    [Orientation.OUEST]: {
        [Mouvement.AVANCER]: Orientation.OUEST,
        D: Orientation.NORD,
        G: Orientation.SUD
    },
    [Orientation.EST]: {
        [Mouvement.AVANCER]: Orientation.EST,
        D: Orientation.SUD,
        G: Orientation.NORD
    },
    [Orientation.SUD]: {
        [Mouvement.AVANCER]: Orientation.SUD,
        D: Orientation.OUEST,
        G: Orientation.EST
    },
    [Orientation.NORD]: {
        [Mouvement.AVANCER]: Orientation.NORD,
        D: Orientation.EST,
        G: Orientation.OUEST
    },
};