import { Subject } from 'rxjs';
import { CarteBuilder } from '../entities/CarteBuilder';
import { Injectable } from '@angular/core';
import axios from 'axios';
import { Carte } from '../entities/Carte/Carte';
import { Case } from '../entities/Case';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class CarteAuTresorService {

  constructor() { }

  getEntry() {
    return axios.get('assets/entry.json')
      .then(({data}) => data.entry)
      .catch((error) => `error occured when getting entry file from asset: ${error}`)
  }

  buildMap(entry: string): Carte {
    const ids = ['C', 'M', 'T', 'A'];
    const splitedEntree = entry.split('\n');
    const carteBuilder = new CarteBuilder();

    splitedEntree
      .map((line) => line.split('-')
      .map((elem) => elem.trim()))
      .forEach((val) => {
          const [id, ...rest] = val;
          ids.includes(id) && carteBuilder.buildMapWith[id] ? carteBuilder.buildMapWith[id](...rest) : null;
      });

    return carteBuilder.build();
  }

  start(carte: Carte, outputMouv$: Subject<Array<Array<Case>>>): void {
    while(carte.aventuriers.some(({seqMouvs}) => seqMouvs.length > 0)) {
      carte.aventuriers.forEach((aventurier) => {
        const mouv = aventurier.seqMouvs.shift()
        if (mouv === 'A') {
          carte.updateAventurierPosition(aventurier, aventurier.orientation);
          outputMouv$.next(_.cloneDeep(carte.map));
        }
        if (['D', 'G'].includes(mouv)) {
          carte.updateAventurierOrientation(aventurier, mouv);
        }
      });
    }
  }


}
