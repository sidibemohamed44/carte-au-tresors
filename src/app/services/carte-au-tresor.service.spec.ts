import { Subject } from 'rxjs';
import { Aventurier, Montagne, Tresor } from './../entities/Element';
import { async, TestBed } from '@angular/core/testing';
import axios from 'axios';

import { CarteAuTresorService } from './carte-au-tresor.service';
import { Carte } from '../entities/Carte/Carte';
import { Case } from '../entities/Case';

let service: CarteAuTresorService;
describe('CarteAuTresorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.get(CarteAuTresorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getEntry', () => {
    it('should get entry data', async () => {
      // Given
      const expectedValue = 'toto';
      spyOn(axios, 'get').and.returnValue(Promise.resolve({data: {entry: expectedValue}}));
  
      // When
      const result = await service.getEntry();
  
      // Then
      expect(result).toEqual(expectedValue);
    });

    it('should throw error', async () => {
        // Given
        const expectedError = 'error occured when getting entry file from asset: error stack';
        spyOn(axios, 'get').and.returnValue(Promise.reject('error stack'));
    
        // When
        const result = await service.getEntry();
    
        // Then
        expect(result).toEqual(expectedError);
    });
  });

  describe('buildMap', () => {
    it('should buildMap', () => {
      // Given
      const entry = `C - 3 - 1
      M - 0 - 0
      T - 1 - 0 - 2
      A - Lara-2-0-S-AADADAGGA`;

      const tresor: Tresor = new Tresor(1, 0, 2);
      const aventurier: Aventurier = new Aventurier('Lara', 2, 0, 'S', 'AADADAGGA');
      const montagne: Montagne = new Montagne(0, 0);

      const cas1: Case = new Case();
      cas1.montagne = montagne;

      const cas2: Case = new Case();
      cas2.tresor = tresor;

      const cas3: Case = new Case();
      cas3.aventurier = aventurier;

      const expectedCarte: Carte = new Carte();
      expectedCarte.x = 3;
      expectedCarte.y = 1;
      expectedCarte.id = 'C';
      expectedCarte.tresors = [tresor];
      expectedCarte.aventuriers = [aventurier];
      expectedCarte.montagnes = [montagne];
      expectedCarte.map = [[cas1, cas2, cas3]]

      // When
      const result: Carte = service.buildMap(entry);

      // Then
      expect(result).toEqual(expectedCarte);
      });

    });

  describe('start', () => {
    it('should execute player moves', () => {
      // Given
      const aventurier1 = new Aventurier('aventurier1', 0, 1, 'S', 'AD');
      const aventurier2 = new Aventurier('aventurier2', 0, 2, 'S', 'AG');
      const carte = new Carte();
      carte.aventuriers = [aventurier1, aventurier2];
      const outputMouv$ = new Subject<any>();
      
      spyOn(outputMouv$, 'next');
      spyOn(carte, 'updateAventurierPosition');
      spyOn(carte, 'updateAventurierOrientation');

      // When
      service.start(carte, outputMouv$);

      //Then
      expect(carte.updateAventurierPosition).toHaveBeenCalledTimes(2);
      expect(carte.updateAventurierOrientation).toHaveBeenCalledTimes(2);
    });

    it('should next updated map with player moves', async(() => {
      // Given
      const aventurier = new Aventurier('aventurier', 0, 1, 'S', 'AD');
      const carte = new Carte();
      carte.map = [buildCaseFixture(2)]
      carte.aventuriers = [aventurier];
      const outputMouv$ = new Subject<any>();
      
      spyOn(outputMouv$, 'next');
      spyOn(carte, 'updateAventurierPosition').and.callFake(() => {carte.map[0][1] = 'toto' as any});
      spyOn(carte, 'updateAventurierOrientation').and.callFake(() => aventurier.orientation = 'ouest');

      // When
      service.start(carte, outputMouv$);

      //Then
      expect(carte.updateAventurierPosition).toHaveBeenCalledTimes(1);
      expect(carte.updateAventurierOrientation).toHaveBeenCalledTimes(1);
      expect(outputMouv$.next).toHaveBeenCalledTimes(1);
      expect(outputMouv$.next).toHaveBeenCalledWith(carte.map);
    }));
  });
});

const buildCaseFixture = (nb) => new Array(nb).fill(new Case());