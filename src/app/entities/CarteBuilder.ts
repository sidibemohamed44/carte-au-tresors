import { Carte } from "./Carte/Carte";
import { Case } from "./Case";
import { Tresor, Montagne, Aventurier } from "./Element";

export class CarteBuilder {
    private carte: Carte;

    constructor() {
        this.carte = new Carte();
    }

    private addTresor(x, y, count) {
        this.carte.tresors.push(new Tresor(x, y, count));
    }

    private addMontagne(x, y) {
        this.carte.montagnes.push(new Montagne(x, y));
    }

    private addAventurier(nom, x, y, orientation, seqMouvs) {
        this.carte.aventuriers.push(new Aventurier(nom, x, y, orientation, seqMouvs));
    }

    buildMapWith = {
        C: (x, y) => {this.carte.x = parseInt(x); this.carte.y = parseInt(y)},
        M: (x, y) => this.addMontagne(x, y),
        T: (x, y, count) => this.addTresor(x, y, count),
        A: (nom, x, y, orientation, seqMouvs) => this.addAventurier(nom, x, y, orientation, seqMouvs)
    }

    build(): Carte {
        for(let i = 0; i < this.carte.y; i++) {
            this.carte.map.push([]);
            for (let j = 0; j < this.carte.x; j++) {
                this.carte.map[i].push(new Case());
            }
        }
        [
            {id: 'tresors', caseSetter: 'setTresor'},
            {id: 'montagnes', caseSetter: 'setMontagne'},
            {id: 'aventuriers', caseSetter: 'setAventurier'}
        ].forEach(({id, caseSetter}) => {
            this.carte[id].forEach(elem => {
                this.carte.map[elem.position.y][elem.position.x][caseSetter](elem);
            });
        })
        return this.carte;
    }

}