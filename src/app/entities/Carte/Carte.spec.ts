import { Case } from '../Case';
import { Aventurier, Montagne, Tresor } from '../Element';
import { Orientation, Mouvement } from './../../helper/mapping';
import { Carte } from './Carte';

describe('Carte', () => {
    describe('updateAventurierOrientation', () => {
        it('should update aventurier\'s orientation ', () => {
            // Given
            const orientation = Orientation.EST;
            const mouvement = Mouvement.DROITE;
            const expected = Orientation.SUD;

            const carte: Carte = new Carte();
            carte.aventuriers = [new Aventurier('toto', 0, 1, orientation, 'AA')];

            // When
            carte.updateAventurierOrientation(carte.aventuriers[0], mouvement);

            // Then
            expect(carte.aventuriers[0].orientation).toEqual(expected);
        });
    });

    describe('updateAventurierPosition', () => {
        describe('update Horizontal Axe', () => {
            it('should increment horizontal axe pos when orientation is Est and move is Avancer', () => {
                // Given
                const orientation = Orientation.EST;
                const carte: Carte = new Carte();
                const aventurier: Aventurier = new Aventurier('toto', 0, 0, orientation, 'AA');
                const cas: Case = new Case();
                cas.aventurier = aventurier;
                carte.map = [[cas, new Case()]];

                const expectedCase = new Case();
                expectedCase.aventurier = aventurier; 

                // When
                carte.updateAventurierPosition(aventurier, orientation);
                // Then
                expect(carte.map[0][1]).toEqual(expectedCase);
                expect(carte.map[0][0].aventurier).toEqual(undefined);
            });

            it('should decrement horizontal axe pos when orientation is Ouest and move is Avancer', () => {
                // Given
                const orientation = Orientation.OUEST;
                const carte: Carte = new Carte();
                const aventurier: Aventurier = new Aventurier('toto', 1, 0, orientation, 'AA');
                const cas: Case = new Case();
                cas.aventurier = aventurier;
                carte.map = [[new Case(), cas]];

                const expectedCase = new Case();
                expectedCase.aventurier = aventurier; 

                // When
                carte.updateAventurierPosition(aventurier, orientation);

                // Then
                expect(carte.map[0][0]).toEqual(expectedCase);
                expect(carte.map[0][1].aventurier).toEqual(undefined);
            });

            it('should ignore move when mouv is out of map ', () => {
                // Given
                const orientation = Orientation.OUEST;
                const carte: Carte = new Carte();
                const aventurier: Aventurier = new Aventurier('toto', 0, 0, orientation, 'AA');
                const cas: Case = new Case();
                cas.aventurier = aventurier;
                carte.map = [[cas]];

                // When
                carte.updateAventurierPosition(aventurier, orientation);

                // Then
                expect(carte.map[0][0]).toEqual(cas);
            });
        });

        describe('update vertical Axe', () => {
            it('should increment vertical axe pos when orientation is Sud and move is Avancer', () => {
                // Given
                const orientation = Orientation.SUD;
                const carte: Carte = new Carte();
                const aventurier: Aventurier = new Aventurier('toto', 0, 0, orientation, 'AA');
                const cas: Case = new Case();
                cas.aventurier = aventurier;
                carte.map = [[cas, new Case()], [new Case(), new Case()]];

                const expectedCase = new Case();
                expectedCase.aventurier = aventurier; 

                // When
                carte.updateAventurierPosition(aventurier, orientation);

                // Then
                expect(carte.map[1][0]).toEqual(expectedCase);
                expect(carte.map[0][0].aventurier).toEqual(undefined);
            });

            it('should decrement vertical axe pos when orientation is Nord and move is Avancer', () => {
                // Given
                const orientation = Orientation.NORD;
                const carte: Carte = new Carte();
                const aventurier: Aventurier = new Aventurier('toto', 0, 1, orientation, 'AA');
                const cas: Case = new Case();
                cas.aventurier = aventurier;
                carte.map = [[new Case(), new Case()], [cas, new Case()]];

                const expectedCase = new Case();
                expectedCase.aventurier = aventurier; 

                // When
                carte.updateAventurierPosition(aventurier, orientation);

                // Then
                expect(carte.map[0][0]).toEqual(expectedCase);
                expect(carte.map[1][0].aventurier).toEqual(undefined);
            });

            it('should ignore move when mouv is out of map ', () => {
                // Given
                const orientation = Orientation.NORD;
                const carte: Carte = new Carte();
                const aventurier: Aventurier = new Aventurier('toto', 0, 0, orientation, 'AA');
                const cas: Case = new Case();
                cas.aventurier = aventurier;
                carte.map = [[cas, new Case()], [new Case(), new Case()]];

                const expectedCase = new Case();
                expectedCase.aventurier = aventurier; 

                // When
                carte.updateAventurierPosition(aventurier, orientation);

                // Then
                expect(carte.map[0][0]).toEqual(expectedCase);
            });
        });

        describe('tresor', () => {
            it('should increment aventurier tresor and decrement case tresor count', () => {
                // Given
                const orientation = Orientation.EST;
                const carte: Carte = new Carte();
                const aventurier: Aventurier = new Aventurier('toto', 0, 0, orientation, 'AA');
                const cas: Case = new Case();
                cas.aventurier = aventurier;

                const cas2: Case = new Case();
                cas2.tresor = new Tresor(1, 0, 2);
                carte.map = [[cas, cas2]];

                const expectedCase = new Case();
                expectedCase.aventurier = aventurier; 
                expectedCase.tresor = new Tresor(1, 0, 1);

                // When
                carte.updateAventurierPosition(aventurier, orientation);

                // Then
                expect(carte.map[0][1]).toEqual(expectedCase);
                expect(aventurier.nbTresors).toEqual(1);
                expect(cas2.tresor.count).toEqual(1);
            });
        });

        describe('Montagne', () => {
            it('should ignore mouvement when there is a mountain on next case', () => {
                // Given
                const orientation = Orientation.EST;
                const carte: Carte = new Carte();
                const aventurier: Aventurier = new Aventurier('toto', 0, 0, orientation, 'AA');
                const cas: Case = new Case();
                cas.aventurier = aventurier;

                const cas2: Case = new Case();
                cas2.montagne = new Montagne(1, 0);
                carte.map = [[cas, cas2]];

                const expectedCase = new Case();
                expectedCase.aventurier = aventurier; 

                // When
                carte.updateAventurierPosition(aventurier, orientation);

                // Then
                expect(carte.map[0][0]).toEqual(expectedCase);
            });
        });

    });
});