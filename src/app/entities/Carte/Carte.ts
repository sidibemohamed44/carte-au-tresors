import _ from 'lodash';

import { Orientation } from "../../helper/mapping";
import { Case } from "../Case";
import { Tresor, Montagne, Aventurier } from "../Element";
import { getOrientationFromThis } from 'src/app/helper/helper';

export class Carte {
    x: number;
    y: number;
    id: string = 'C';
    tresors: Tresor[] = [];
    montagnes: Montagne[] = [];
    aventuriers: Aventurier[] = [];
    map: Array<Array<Case>> = [];

    public updateAventurierOrientation(aventurier, nextMouv) {
      aventurier.orientation = getOrientationFromThis(aventurier.orientation).forThis(nextMouv);
    }

    private updateHorizontalAxe(aventurier, nextAxeHorz) {
        if (!(_.get(this.map, [aventurier.position.y, nextAxeHorz])) || 
            this.map[aventurier.position.y][nextAxeHorz].montagne || 
            this.map[aventurier.position.y][nextAxeHorz].aventurier){
            return;
        }
        this.map[aventurier.position.y][aventurier.position.x].aventurier = undefined;
        aventurier.position.x = nextAxeHorz;
        this.map[aventurier.position.y][nextAxeHorz].aventurier = aventurier;

        const tresor: Tresor = this.map[aventurier.position.y][nextAxeHorz].tresor;
        if (tresor) {
            tresor.count--;
            aventurier.nbTresors++;
            this.map[aventurier.position.y][nextAxeHorz].tresor = tresor.count ? tresor : undefined;
        }
    }

    private updateVerticalAxe(aventurier, nextAxeVert) {
        if (!(_.get(this.map, [nextAxeVert, aventurier.position.x])) ||
            this.map[nextAxeVert][aventurier.position.x].montagne ||
            this.map[nextAxeVert][aventurier.position.x].aventurier) {
            return; 
        }
        this.map[aventurier.position.y][aventurier.position.x].aventurier = undefined;
        aventurier.position.y = nextAxeVert;
        this.map[nextAxeVert][aventurier.position.x].aventurier = aventurier;

        const tresor: Tresor = this.map[nextAxeVert][aventurier.position.x].tresor;
        if (tresor) {
            tresor.count--;
            aventurier.nbTresors++;
            this.map[nextAxeVert][aventurier.position.x].tresor = tresor.count ? tresor : undefined;
        }
    }

    public updateAventurierPosition(aventurier, currentOrientation) {
        switch(currentOrientation) {
            case Orientation.OUEST:
                const nextAxeHorzO = aventurier.position.x - 1;
                this.updateHorizontalAxe(aventurier, nextAxeHorzO);
                break;
            
            case Orientation.EST: 
                const nextAxeHorzE = aventurier.position.x + 1;
                this.updateHorizontalAxe(aventurier, nextAxeHorzE);
                break;

            case Orientation.NORD: 
                const nextAxeVertN = aventurier.position.y - 1;
                this.updateVerticalAxe(aventurier, nextAxeVertN);
                break;

            case Orientation.SUD: 
                const nextAxeVertS = aventurier.position.y + 1;
                this.updateVerticalAxe(aventurier, nextAxeVertS);
                break;
        }
    }
    
};