import { Tresor, Montagne, Aventurier } from "./Element";

export class Case {
    empty = '.';
    tresor: Tresor;
    montagne: Montagne;
    aventurier: Aventurier;

    setTresor(tresor: Tresor) {
        this.tresor = tresor;
    }

    setMontagne(montange: Montagne) {
        this.montagne = montange;
    }

    setAventurier(aventurier: Aventurier) {
        this.aventurier = aventurier;
    }

    toString() {
        return (this.montagne && this.montagne.toString()) ||
            (this.tresor && this.aventurier ? `${this.tresor.toString()} ${this.aventurier.toString()}` : undefined) ||
            (this.tresor && this.tresor.toString()) ||
            (this.aventurier && this.aventurier.toString()) ||
            this.empty;
    }
}