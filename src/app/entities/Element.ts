export class Position {
    x: number;
    y: number;
    constructor(x, y) {
        this.x = parseInt(x);
        this.y = parseInt(y);
    }
};

export class Montagne {
    position: Position;
    id: string = 'M';
    constructor(x, y) {
        this.position = new Position(x, y);
     }

    toString() {
        return `${this.id}`;
    }
}

export class Tresor {
    position: Position;
    id: string = 'T';
    count: number;
    constructor(x, y, count) {
        this.count = parseInt(count);
        this.position = new Position(x, y);
    }

    toString() {
        return `${this.id}(${this.count})`;
    }
}

export class Aventurier {
    position: Position;
    id: string = 'A';
    nom: string;
    orientation: string;
    seqMouvs: string[];
    nbTresors: number = 0;

    constructor(nom, x, y, orientation, seqMouvs) {
        this.position = new Position(x, y);
        this.nom = nom;
        this.orientation = orientation;
        this.seqMouvs = seqMouvs.split('');
    }

    toString() {
        return `${this.id}(${this.nom})`;
    }
}