import _ from 'lodash';

import { CarteAuTresorService } from './services/carte-au-tresor.service';
import { Subject } from 'rxjs';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Carte } from './entities/Carte/Carte';
import { Case } from './entities/Case';
import { getRatio } from './helper/helper';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'La carte au trésors';
  render$ = new Subject<Array<Array<Case>>>();
  private carte: Carte;
  error: string;

  @ViewChild('canvas') 
  canvas: ElementRef<HTMLCanvasElement>;
  private ctx: CanvasRenderingContext2D;
  private i = 1;

  constructor(private carteAuTresorService: CarteAuTresorService) {}

  async ngOnInit(): Promise<void> {
    this.ctx = this.canvas.nativeElement.getContext('2d');
    this.ctx.font = '20px serif';
    try {
      const entryData = await this.carteAuTresorService.getEntry();
      this.carte = this.carteAuTresorService.buildMap(entryData);

      this.render$.pipe().subscribe((map: Array<Array<Case>>) => {
        setTimeout(() => {
          this.renderMap(map);
        }, 1000 * this.i);
        this.i++;
      });

      this.render$.next(_.cloneDeep(this.carte.map));
      this.carteAuTresorService.start(this.carte, this.render$);
    } 
    catch (error) {
      this.error = error;
    }

  }

  renderMap(map) {
    this.clear(0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
    map.forEach((elem, topIndex) => {
      elem.forEach((innerElem: Case, innerIndex) => {
        const x = (getRatio(innerIndex ,  elem.length - 1) * (this.canvas.nativeElement.width - 20)) / 100;
        const y = ((getRatio(topIndex, this.carte.map.length - 1) * (this.canvas.nativeElement.height - 20)) / 100) + 14;
        this.drawText(x, y, innerElem);
      });
    });
  }

  private drawText(x: number, y: number, elem: Case) {
    this.ctx.fillText(elem.toString(), x, y, 100);
  }

  private clear(x: number, y: number, w: number, h: number) {
    this.ctx.clearRect(x, y, w, h);
  }
  
}
