import { CarteAuTresorService } from './services/carte-au-tresor.service';
import { TestBed, async, ComponentFixture, fakeAsync, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { Case } from './entities/Case';

const serviceMock = {
  getEntry: jasmine.createSpy('getEntry'),
  buildMap: jasmine.createSpy('buildMap'),
  start: jasmine.createSpy('start')
};

let fixture: ComponentFixture<AppComponent>;

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        {provide: CarteAuTresorService, useValue: serviceMock}
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(AppComponent);
  }));

  it('should create the app', () => {
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'La carte au trésors'`, () => {
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('La carte au trésors');
  });

  it('should render title in a h1 tag', () => {
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h4').textContent).toContain('La carte au trésors');
  });

  describe('ngOnInit', () => {
    beforeEach(() => {
      serviceMock.start.calls.reset();
      serviceMock.getEntry.calls.reset();
      serviceMock.buildMap.calls.reset();
    });

    it('should initialize carte and start painting the result', fakeAsync(() => {
      // Given
      const entry = 'entry';
      const fakeCarte = { map: [[new Case()]]};
  
      serviceMock.getEntry.and.returnValue(Promise.resolve(entry));
      serviceMock.buildMap.and.returnValue(fakeCarte);
      const app: AppComponent = fixture.debugElement.componentInstance;
  
      spyOn(app, 'renderMap');
      // When
      app.ngOnInit();
  
      // Then
  
      tick(1000);
  
      expect(serviceMock.getEntry).toHaveBeenCalledTimes(1);
      expect(serviceMock.buildMap).toHaveBeenCalledWith(entry);
      expect(serviceMock.start).toHaveBeenCalledTimes(1);
      expect(app.renderMap).toHaveBeenCalledTimes(1);
  
    }));
  });

});
